import { Button, Flex, Image, Link, Stack, Text } from '@chakra-ui/react'
import { Link as RouterLink } from 'react-router-dom'

export default function Hero() {
  return (
    <Flex
      justifyContent={'center'}
      alignItems={'center'}
      my={'4rem'}
      backgroundColor={'background'}
      minH={'500px'}
    >
      <Flex w={'500px'} h={'full'} alignItems={'center'}>
        <Image
          src="https://via.placeholder.com/325x467/92c952"
          alt="cta photo"
        />
      </Flex>
      <Stack
        direction="column"
        w={'500px'}
        spacing={8}
        justifyContent={'center'}
      >
        <Text fontSize={'5xl'}>Explore Our Spring Designs</Text>
        <Text fontSize={'xl'}>
          facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris
          rhoncus aenean vel elit scelerisque mauris pellentesque pulvinar
          pellentesque habitant morbi tristique senectus et netus
        </Text>
        <Link as={RouterLink} to="/products">
          <Button
            as="a"
            cursor="pointer"
            bg={'primary'}
            color={'white'}
            minW={'150px'}
            borderRadius={0}
          >
            Shop Now
          </Button>
        </Link>
      </Stack>
    </Flex>
  )
}
