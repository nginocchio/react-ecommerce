import { EmailIcon, InfoIcon, PhoneIcon } from '@chakra-ui/icons'
import { Box, SimpleGrid, Stack, Text, Flex } from '@chakra-ui/react'
import { Link } from 'react-router-dom'
import { FiFacebook, FiTwitter } from 'react-icons/fi'
import { FiInstagram } from 'react-icons/fi'
import { FaPinterestP } from 'react-icons/fa'

export default function Footer() {
  return (
    <Box bg={'background'}>
      <SimpleGrid
        columns={[1, null, 3]}
        w="container.xl"
        m="auto"
        spacing={10}
        p={5}
        color="black"
      >
        <FooterAbout />
        <FooterLinks />
        <FooterContact />
      </SimpleGrid>
    </Box>
  )
}

function FooterAbout() {
  return (
    <Stack direction={'column'} spacing={4}>
      <Text fontFamily={'heading'} fontWeight={'bold'} fontSize={'4xl'}>
        Logo.
      </Text>
      <Box as={'h4'} fontSize={'1.5rem'}>
        Address
      </Box>
      <Text>622 Dixie Path, South Tobinchester 98336</Text>
      <Box as={'h4'} fontSize={'1.5rem'}>
        Store Hours
      </Box>
      <Text>Monday - Saturday</Text>
      <Text>10am - 10pm</Text>
    </Stack>
  )
}

function FooterLinks() {
  return (
    <Stack direction={'column'} spacing={4}>
      <Text fontFamily={'heading'} fontSize={'2xl'}>
        Get In Touch
      </Text>
      <SimpleGrid columns={2} spacing={1}>
        <Text>Phone</Text>
        <Text>012-345-6789</Text>
        <Text>Support</Text>
        <Text>012-345-6789</Text>
        <Text>Customer Service</Text>
        <Text>012-345-6789</Text>
      </SimpleGrid>
      <Stack direction={'row'} spacing={2}>
        <Flex
          w={'50px'}
          height={'50px'}
          borderRadius={'50%'}
          bg={'white'}
          justifyContent={'center'}
          alignItems={'center'}
        >
          <FiFacebook style={{ color: '#D84727', fontSize: '1.5em' }} />
        </Flex>
        <SocialIcon
          icon={<FiInstagram style={{ color: '#D84727', fontSize: '1.5em' }} />}
          bgColor="white"
        />
        <SocialIcon
          icon={<FiTwitter style={{ color: '#D84727', fontSize: '1.5em' }} />}
          bgColor="white"
        />
        <SocialIcon
          icon={
            <FaPinterestP style={{ color: '#D84727', fontSize: '1.5em' }} />
          }
          bgColor="white"
        />
      </Stack>
    </Stack>
  )
}

function FooterContact() {
  return (
    <Stack direction={'column'} spacing={4}>
      <Text fontFamily={'heading'} fontSize={'2xl'}>
        Useful Links
      </Text>
      <Text fontSize={'1rem'}>Careers</Text>
      <Text fontSize={'1rem'}>FAQ</Text>
      <Text fontSize={'1rem'}>About Us</Text>
      <Text fontSize={'1rem'}>Careers</Text>
    </Stack>
  )
}

function SocialIcon({ icon, bgColor }: { icon: JSX.Element; bgColor: string }) {
  return (
    <Flex
      w={'50px'}
      height={'50px'}
      borderRadius={'50%'}
      bg={'white'}
      justifyContent={'center'}
      alignItems={'center'}
    >
      {icon}
    </Flex>
  )
}
