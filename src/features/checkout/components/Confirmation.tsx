import { CheckIcon } from '@chakra-ui/icons'
import { Box, Button, Heading, Text } from '@chakra-ui/react'

const orderNumber = 'pz12xv7540q1'

export default function Confirmation() {
  return (
    <Box maxW={'800px'} margin={'auto'}>
      <Heading>Your Order has been received!</Heading>
      <CheckIcon color={'green'} />
      <Text>Thank you for your purchase</Text>
      <Text>Your order number is: {orderNumber}</Text>
      <Text>You should receive an email confirmation shortly</Text>
      <Button>Continue shopping</Button>
    </Box>
  )
}
