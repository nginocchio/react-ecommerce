import { ContentLayout } from '../../../components/ContentLayout'
import Confirmation from '../components/Confirmation'

export const ConfirmRoute = () => {
  return (
    <ContentLayout title={'Checkout confirmed'}>
      <Confirmation />
    </ContentLayout>
  )
}
