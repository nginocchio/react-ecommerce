import { MyState } from 'src/store/useStore'
import { SetState } from 'zustand'

const STRIPE_PK =
  'pk_test_51K5IwLLxHIUxOV4LYb9PrrmwfIYBiSj1CH5HrCyNph2KbIFwxhLu5PtcoNHanDHKmZ0ic3wYEWs0Imx4XPgeu0bZ00MlmzMi9h'

export interface CheckoutSlice {
  clientSecret?: string
  // setClientSecret: (secret: string) => void
}

export const createCheckoutSlice = (set: SetState<MyState>) => ({
  clientSecret: undefined,
  // setClientSecret: (secret: string) => set({ clientSecret: secret }),
})
