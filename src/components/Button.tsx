import { Button as ChakraButton } from '@chakra-ui/react'

export type ButtonProps = {
  buttonText: string
  [key: string]: any
}

export default function Button({ buttonText, ...props }: ButtonProps) {
  return (
    <ChakraButton
      {...props}
      cursor="pointer"
      bg={'primary'}
      color={'white'}
      minW={'150px'}
      borderRadius={0}
    >
      {buttonText}
    </ChakraButton>
  )
}
