import { Box, Flex, Heading, Text, Stack, Link, Image } from '@chakra-ui/react'

import { Link as RouterLink } from 'react-router-dom'
import Navbar from './Navbar'
import Hero from './Hero'
import Footer from './Footer'
import { useState } from 'react'

export default function Home() {
  return (
    <Box>
      <Navbar />
      <Box maxW={'container.xl'} mx={'auto'}>
        <Hero />
        <Stack spacing={6} direction={'row'}>
          <FeaturedProduct category="Explore Eyewear" />
          <FeaturedProduct category="Explore Watches" />
        </Stack>
        <LatestDeals />
        <Footer />
      </Box>
    </Box>
  )
}

function FeaturedProduct({ category }: { category: string }) {
  return (
    <Flex
      w="full"
      h="270px"
      direction="row"
      p={5}
      bg="white"
      position={'relative'}
    >
      <Flex w={'full'} direction="column" justifyContent={'start'}>
        <Text fontSize={'lg'} mb={1}>
          {category}
        </Text>
        <Text maxW={'350px'}>
          See the beauty of the exotic world with these new luxorious glasses
        </Text>
        <Link as={RouterLink} to="/products" mt={3}>
          Discover Now
        </Link>
      </Flex>
      <Image
        src="https://via.placeholder.com/325x150/92c952"
        alt="category photo"
        maxH={'150px'}
        position="absolute"
        bottom={'10px'}
        right={'10px'}
      />
    </Flex>
  )
}

function NewProductCard({
  img,
  name,
  discount,
  price,
}: {
  img: string
  name: string
  discount: number
  price: number
}) {
  const property = {
    imageUrl: 'https://bit.ly/2Z4KKcF',
    imageAlt: 'Rear view of modern home with pool',
    reviewCount: 34,
    rating: 4,
  }
  const [buttonsVisible, showButtons] = useState<Boolean>(false)

  return (
    <Box
      minW="255px"
      bg={'white'}
      onMouseEnter={() => showButtons(true)}
      onMouseLeave={() => showButtons(false)}
    >
      <Image
        src={img}
        alt={property.imageAlt}
        // onClick={() => navigate(`/products/${id}`)}
        // onMouseEnter={() => showButtons(true)}
        // onMouseLeave={() => showButtons(false)}
        cursor={'pointer'}
        mx="auto"
      />

      <Box p="6">
        {/* <Box display="flex" alignItems="baseline">
          <Badge borderRadius="full" px="2" colorScheme="teal">
            New
          </Badge>
        </Box>
 */}
        <Box
          mt="1"
          fontWeight="medium"
          as="h2"
          lineHeight="tight"
          isTruncated
          fontSize={'24px'}
        >
          {name}
        </Box>

        <Box>{discount * 100}% off</Box>
        <Box>${price}</Box>
        {buttonsVisible ? <ProductActions /> : ''}
      </Box>
    </Box>
  )
}

function ProductActions() {
  return <Text>Product actions</Text>
}

function LatestDeals() {
  return (
    <Box py={5}>
      <Text mb={2} fontSize={'4xl'}>
        Latest Deals
      </Text>
      <Stack justifyContent={'space-between'} direction={'row'} w={'full'}>
        <NewProductCard
          img="https://via.placeholder.com/210x210/92c952"
          name="Some Product"
          discount={0.2}
          price={89.99}
        />
        <NewProductCard
          img="https://via.placeholder.com/210x210/92c952"
          name="Some Product"
          discount={0.2}
          price={89.99}
        />
        <NewProductCard
          img="https://via.placeholder.com/210x210/92c952"
          name="Some Product"
          discount={0.2}
          price={89.99}
        />
        <NewProductCard
          img="https://via.placeholder.com/210x210/92c952"
          name="Some Product"
          discount={0.2}
          price={89.99}
        />
      </Stack>
    </Box>
  )
}
