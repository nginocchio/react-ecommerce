import { extendTheme } from '@chakra-ui/react'

export const theme = extendTheme({
  colors: {
    primary: '#D84727',
    background: '#F1DDC9',
  },
  fonts: {
    heading: 'taviraj, serif',
    body: 'taviraj, serif',
  },
  fontSizes: {
    lg: '2.25rem',
    xl: '3.75',
  },
  styles: {
    global: (props) => ({
      body: {
        bg: '#F7F6F4',
      },
    }),
  },
})
